package crudoperation;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class UpdateFrame {

	public UpdateFrame() {
		JFrame frame=new JFrame("Update Details");
		 frame.setSize(400,500);
		 
		 JLabel ID=new JLabel("ID");
		 ID.setBounds(23,45,100,30);
		 frame.add(ID);
		  
		 JTextField IDField=new JTextField();
		 IDField.setBounds(150,45,100,30);
		 frame.add(IDField);
		 
		 JLabel Student_name=new JLabel("Student name");
		 Student_name.setBounds(23,100,150,30);
		 frame.add(Student_name);
		 
		 JTextField Student_nameField=new JTextField();
		 Student_nameField.setBounds(150,100,100,30);
		 frame.add(Student_nameField);
		  
		 JLabel city_name=new JLabel("Enter student city");
		 city_name.setBounds(23,150,100,30);
		 frame.add(city_name);
		 
		 JTextField city_nameField=new JTextField();
		 city_nameField.setBounds(150,150,100,30);
		 frame.add(city_nameField);
		  
		 JButton Update=new JButton();
		 Update.setText("Update");
		 Update.setBounds(150,200,100,30);
	 	 Update.addActionListener(new ActionListener()
	 	 {  
			 public void actionPerformed(ActionEvent e)
			 {  
				 try
				 {  
					 Class.forName("oracle.jdbc.driver.OracleDriver");  
					 Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","system","kalajayapal");  
					 String studentname=Student_nameField.getText();
					 int Id=Integer.parseInt(IDField.getText());
					 String city=city_nameField.getText();
					 PreparedStatement stmt=con.prepareStatement("update student_details set name='"+studentname+"',city='"+city+"' where id="+Id);
					 int i=stmt.executeUpdate();  
					 JOptionPane.showMessageDialog(frame,i+"record updated"); 
					 System.out.println(i+" records updated");    
					 con.close();  
				 }
				 catch(Exception e1)
				 { 
				  System.out.println(e1);}  
				 }
		  });
	 frame.add(Update);
	 frame.setLayout(null);
	 frame.setVisible(true);
	}
	}

