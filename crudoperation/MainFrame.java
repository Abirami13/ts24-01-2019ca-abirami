package crudoperation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class MainFrame {

	public static void main(String[] args) {
		JFrame frame=new JFrame("CRUD OPERATION");
		 frame.setSize(400,500);
		 JButton AddButton=new JButton();
		 AddButton.setText("Add");
		 AddButton.setBounds(100,50,95,30);
		 frame.add(AddButton); 
		 
		 AddButton.addActionListener(new ActionListener()
		 {  
			 public void actionPerformed(ActionEvent e)
			 {  
				 AddFrame Add=new AddFrame();
				 frame.dispose();
		             
		     }  
		 });  
		 
		 JButton UpdateButton=new JButton();
		 UpdateButton.setText(" Update");
		 UpdateButton.setBounds(100,100,95,30);
		 frame.add(UpdateButton);
		 
		 UpdateButton.addActionListener(new ActionListener()
		 {  
			 public void actionPerformed(ActionEvent e)
			 {  
				 UpdateFrame Update=new UpdateFrame();
				 frame.dispose();  
		     }  
		 });  
		 
		 
		 JButton DeleteButton=new JButton();
		 DeleteButton.setText("Delete");
		 DeleteButton.setBounds(100,150,95,30);
		 frame.add(DeleteButton);
		 
		 DeleteButton.addActionListener(new ActionListener()
		 {  
			 public void actionPerformed(ActionEvent e)
			 {  
				 DeleteFrame Delete=new DeleteFrame();
		         
		     }  
		 }); 
		 frame.setLayout(null); 
		 frame.setVisible(true);
	     frame.setSize(400,500);  
			
	  }
      }
		 
		 
	
